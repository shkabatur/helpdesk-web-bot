import requests

STATUS_OPEN = 31


def get_tasks(auth_token):
    url = "http://help.artek.org/mobi/api/task?serviceid=38&pagesize=100"
    headers = {"Authorization": auth_token}
    try:
        r = requests.get(url, headers=headers)
        tasks = r.json()['Tasks']
    except:
        return []
    return tasks

def get_open_tasks(auth_token):
    tasks = get_tasks(auth_token)
    return [t for t in tasks if t['StatusId'] == STATUS_OPEN]