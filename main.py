import telebot
import os
import threading
import time
import json

from dotenv import load_dotenv
from helpdesk_api import get_open_tasks


load_dotenv()
BOT_API = os.getenv("BOT_API")
DEBUG_API = os.getenv("DEBUG_API")
HELPDESK_PASSWORD = os.getenv("HELPDESK_PASSWORD")

with open("subscribed_user_ids.json", "r") as f:
    subscribed_user_ids = json.load(f)

bot = telebot.TeleBot(BOT_API, parse_mode=None)

@bot.message_handler(commands=["start"])
def start(message):
    bot.send_message(message.from_user.id, "type /subscribe")

@bot.message_handler(commands=["subscribe"])
def subscribe(message):
    id = message.from_user.id
    if id in subscribed_user_ids:
        resp_message = "Ты уже подписан!"
    else:
        subscribed_user_ids.append(id)
        with open("subscribed_user_ids.json", "w") as f:
            f.write(json.dumps(subscribed_user_ids))
        resp_message = "Ура!"
    bot.send_message(id, resp_message)

def my_polling():
    while True:
        try:
            bot.polling()
        except:
            continue

polling = threading.Thread(target=my_polling)
polling.start()

open_task_ids = []

while True:
    try:
        tasks = get_open_tasks(HELPDESK_PASSWORD)
        message = ""
        for task in tasks:
            if task["Id"] in open_task_ids:
                continue
            else:
                message = f'В системе новая заявка:\nhttp://help.artek.org/Task/View/{task["Id"]}\n\n'
                message += task["Name"] + "\n"
                message += task["Description"]
                message += "\n\n\n"
                open_task_ids.append(task["Id"])
        if message:
            for id in subscribed_user_ids:
                bot.send_message(id, message)
    except Exception as e:
        print("Error: {}".format(str(e)))
        continue
    time.sleep(5)
    